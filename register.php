<?php
$sex = array(
    "Nam",
    "Nữ"
);

$department = array(
    "MAT" => "Khoa học máy tính",
    "KDL" => "Khoa học vật liệu"
);

?>

<?php
$error = "";

function validateDate($date, $format = 'd/m/Y')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) === $date;
}



if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $check = true;
    $data = ['uploadimageType' => strtolower(pathinfo($_FILES['img']['name'],PATHINFO_EXTENSION))];
    $ext = array("png","jpg","jpeg");
    if(!in_array($data['uploadimageType'],$ext) ) {
        $error .= "Vui lòng chọn ảnh và đúng định dạng !.<br>";
        $check = false;
    }else {
        $check = true;
    }

    if (!file_exists('/home/vinh/Desktop/day05/upload')) {
        mkdir('/home/vinh/Desktop/day05/upload', 0777, true);
    }

    if (!isset($_POST['name']) || $_POST['name'] == null) {
        $error .= "Hãy nhập tên.<br>";
    }

    if (!isset($_POST['sex']) || $_POST['sex'] == null) {
        $error .= "Hãy chọn giới tính.<br>";
    }

    if (!isset($_POST['department']) || $_POST['department'] == null) {
        $error .= "Hãy chọn phân khoa.<br>";
    }

    if (!isset($_POST['birthday']) || $_POST['birthday'] == null) {
        $error .= "Hãy nhập ngày sinh.<br>";
    } else if (!validateDate($_POST['birthday'])) {
        $error .= "Hãy nhập ngày sinh đúng định dạng.";
    }

    # post anh encode base 64
    $_POST['file'] = $_FILES;

    # submit anh qua display_info bang link directory img
    $temp = explode(".", $_FILES["img"]["name"]);
    $date = date("_YmdHis");
    $newfilename = pathinfo($_FILES['img']['name'],PATHINFO_FILENAME).$date.'.'. end($temp);
    $fileUploaded = "upload/". $newfilename;
    $_POST['img_dir'] = $fileUploaded;

    session_start();
    $_SESSION = $_POST;

    if($_POST['name'] !== null && $_POST['sex'] !== null && $_POST['department'] !== null && $_POST['birthday'] !== null && validateDate($_POST['birthday']) && $check==true){
        move_uploaded_file($_FILES['img']['tmp_name'], $fileUploaded);
        header("Location: display_info.php");
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>

    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <title>Day 05
        
    </title>
</head>

<body>
    <script type="text/javascript">
        $(function() {
            $('#type_date').datepicker({
                format: "dd/mm/yyyy"
            });
        });
    </script>


    <div class="wrapper">
        <form method="POST" enctype="multipart/form-data">
            <div class="error">
                <?php
                echo $error
                ?>
            </div>

            <div class="name__input">
                <label class="star">Họ và tên</label>
                <input class="input_name" type="text" name="name">
            </div>

            <div class="sex__input">
                <label class="star">Giới tính</label>
                <?php
                for ($i = 0; $i < count($sex); $i++) {
                    echo "
                            <input value=\"$i\" class=\"input_sex\" name = \"sex\" type=\"radio\">{$sex[$i]}
                        ";
                }
                ?>
            </div>

            <div class="department__input">
                <label class="star">Phân khoa</label>
                
                <select name="department">
                    <option value=""></option>
                    <?php
                    foreach (array_keys($department) as $dep) {
                        echo '
                                <option value="'.$dep.'">' . $department[$dep] . '</option>
                            ';
                    }
                    ?>
                </select>
            
            </div>

            <div class="date__input">
                <label class="star">Ngày sinh</label>
                <input id="type_date" type="text" class="input_date" name="birthday" placeholder="dd/mm/yyyy" />
            </div>

            <div class="address__input">
                <label>Địa chỉ</label>
                <input class="input_name" type="text" name="address">
            </div>

            <div class="img__input">
                <label for="">Hình ảnh</label>
                <input type='file' name="img" onchange="readURL(this);" />
                <!-- <img id="blah" alt="your image" style="display: none;"/> -->
            </div>
            <input class="img_encode" name="img_encode" style="display: none;"/>

            <button type="submit">Đăng ký</button>
        </form>
    </div>

    <script type="text/javascript">
        function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    $('#blah').css("display","block");
                    reader.onload = function (e) {
                        $('#blah')
                            .attr('src', e.target.result);
                        
                            $('.img_encode').val(e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);
                }else{
                    $('#blah').css("display","none");
                }
            }

    </script>

</body>

</html>
